package sec08.ex04.utilitario;

public class ConversorMoedas {

    public static double iof = 0.06;

    public static double dolarParaReal(double montante, double valorDolar) {
        return montante * valorDolar + (montante * valorDolar * iof) ;
    }

}
