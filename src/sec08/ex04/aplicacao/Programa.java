package sec08.ex04.aplicacao;

import sec08.ex04.utilitario.ConversorMoedas;

import java.util.Scanner;
import java.util.Locale;

public class Programa {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        double precoDolar, montante;

        System.out.println("Digite o preço do Dolar: ");
        precoDolar = sc.nextDouble();
        System.out.println("Digite o valor que deseja comprar: ");
        montante = sc.nextDouble();

        System.out.println();
        System.out.printf("Valor total a ser pago = %.2f%n", ConversorMoedas.dolarParaReal(montante, precoDolar));

        sc.close();

    }

}
