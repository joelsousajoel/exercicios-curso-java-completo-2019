/*
* Fazer um programa para ler o nome de um aluno e as três notas que ele obteve nos três trimestres do ano
* (primeiro trimestre vale 30 e o segundo e terceiro valem 35 cada). Ao final, mostrar qual a nota final do
 * aluno no ano. Dizer também se o aluno está aprovado (PASS) ou não (FAILED) e, em caso negativo, quantos
 * pontos faltam para o aluno obter o mínimo para ser aprovado (que é 60% da nota). Você deve criar uma classe
 * Student para resolver este problema.
* */

package sec08.ex03.aplicacao;

import java.util.Scanner;
import java.util.Locale;
import sec08.ex03.entidade.Aluno;

public class Programa {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);
        Aluno aluno = new Aluno();

        System.out.print("Digite o nome do aluno: ");
        aluno.nome = sc.nextLine();
        System.out.println("Digite as notas dos 3 trimestres: ");
        aluno.trimestre1 = sc.nextDouble();
        aluno.trimestre2 = sc.nextDouble();
        aluno.trimestre3 = sc.nextDouble();

        System.out.println();
        System.out.println(aluno);

        sc.close();

    }

}
