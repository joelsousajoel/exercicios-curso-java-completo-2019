package sec08.ex03.entidade;

public class Aluno {

    public String nome;
    public Double trimestre1;
    public Double trimestre2;
    public Double trimestre3;

    public double notaFinal() {
        return trimestre1 + trimestre2 + trimestre3;
    }

    public String resultado() {
        if (notaFinal() >= 60) {
            return "Aprovado";
        }
        else {
            return String.format("Reprovado%nFaltou %.2f pontos.", 60 - notaFinal());
        }
    }

    public String toString() {
        return String.format("Nota final: %.2f %n%s", notaFinal(), resultado());
    }

}
