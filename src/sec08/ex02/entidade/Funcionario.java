package sec08.ex02.entidade;

public class Funcionario {

    public String nome;
    public Double salarioBruto;
    public Double imposto;

    public double salarioLiquido() {
        return salarioBruto - imposto;
    }

    public void aumentarSalario(double porcentagem) {
        salarioBruto += (salarioBruto * porcentagem) / 100;
    }

    public String toString() {
        return "Funcionário: " + nome +  ", $ " + String.format("%.2f", salarioLiquido());
    }

}
