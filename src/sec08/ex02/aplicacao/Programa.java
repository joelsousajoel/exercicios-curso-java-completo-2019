/*
* Fazer um programa para ler os dados de um funcionário (nome, salário bruto e imposto). Em
* seguida, mostrar os dados do funcionário (nome e salário líquido). Em seguida, aumentar o
* salário do funcionário com base em uma porcentagem dada (somente o salário bruto é
* afetado pela porcentagem) e mostrar novamente os dados do funcionário.
* */

package sec08.ex02.aplicacao;

import sec08.ex02.entidade.Funcionario;
import java.util.Locale;
import java.util.Scanner;

public class Programa {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);
        Funcionario funcionario = new Funcionario();

        System.out.print("Digite o nome do funcionário: ");
        funcionario.nome = sc.nextLine();
        System.out.printf("Digite o salário bruto de %s: ", funcionario.nome);
        funcionario.salarioBruto = sc.nextDouble();
        System.out.print("Digite o valor do imposto: ");
        funcionario.imposto = sc.nextDouble();

        System.out.println();
        System.out.println(funcionario);

        System.out.println();
        System.out.print("Informe a porcentagem para aumentar o salário: ");
        funcionario.aumentarSalario(sc.nextDouble());

        System.out.println();
        System.out.println("Salário atualizado: " + funcionario);

        sc.close();

    }

}
