/*
* Fazer um programa para ler os valores da largura e altura
* de um retângulo. Em seguida, mostrar na tela o valor de
* sua área, perímetro e diagonal. Usar uma classe como
* mostrado no projeto ao lado.
* */

package sec08.ex01.aplicacao;

import sec08.ex01.entidade.Retangulo;
import java.util.Scanner;
import java.util.Locale;

public class Programa {

    public static void main(String[] args) {

        Locale.setDefault(Locale.US);
        Scanner sc = new Scanner(System.in);

        Retangulo retangulo = new Retangulo();

        System.out.println("Digite a altura e largura do retângulo: ");
        retangulo.altura = sc.nextDouble();
        retangulo.largura = sc.nextDouble();

        System.out.printf("Área = %.2f%n", retangulo.area());
        System.out.printf("Perímetro = %.2f%n", retangulo.perimetro());
        System.out.printf("Diagonal = %.2f%n", retangulo.diagonal());

        sc.close();

    }

}
