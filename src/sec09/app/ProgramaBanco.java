package sec09.app;

import java.util.Scanner;
import sec09.entidades.Conta;

public class ProgramaBanco {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        Conta c1;

        double saldo;

        System.out.print("Digite o número da conta: ");
        int numeroConta = sc.nextInt();
        sc.nextLine();
        System.out.print("Digite o nome do titular: ");
        String titular = sc.nextLine();
        System.out.print("Será feito um depósito inicial?(s/n): ");
        String depositoInicial = sc.next();

        if (depositoInicial.equals("s")) {
            System.out.print("Digite o valor do Depósito: ");
            saldo = sc.nextDouble();
            c1 = new Conta(numeroConta, titular, saldo);
        }
        else {
            c1 = new Conta(numeroConta, titular);
        }




        System.out.println();
        System.out.println("Informações da Conta:\n" + c1);

        System.out.println();
        System.out.println("Dite o valor para fazer um depósito: ");
        c1.deposito(sc.nextDouble());

        System.out.println();
        System.out.println("Atualização da Conta:\n" + c1);

        System.out.println();
        System.out.println("Dite o valor para fazer um saque: ");
        c1.saque(sc.nextDouble());

        System.out.println();
        System.out.println("Atualização da Conta:\n" + c1);




        sc.close();

    }

}
