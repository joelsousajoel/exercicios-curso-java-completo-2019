package sec09.entidades;

public class Conta {

    protected int numeroConta;
    private String titular;
    private double saldo;

    public Conta(int numeroConta, String titular, double saldo) {
        this.numeroConta = numeroConta;
        this.titular = titular;
        deposito(saldo);
    }

    public Conta(int numeroConta, String titular) {
        this.numeroConta = numeroConta;
        this.titular = titular;
    }

    public String getTitular() {
        return titular;
    }

    public void setTitular(String titular) {
        this.titular = titular;
    }

    public void deposito(double deposito) {
        saldo += deposito;
    }

    public void saque(double saque) {
        saldo -= saque;
    }

    public String toString() {

            return "Num. Conta: " + numeroConta + ", "
                    + "Titular: " + titular + ", "
                    + "Saldo: " + saldo;

    }

}


