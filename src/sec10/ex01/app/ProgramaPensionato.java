package sec10.ex01.app;

import java.util.Scanner;
import sec10.ex01.entidades.Reserva;

public class ProgramaPensionato {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        Reserva[] reserva = new Reserva[10];

        System.out.print("Quantos quartos serão alugados? ");
        int quantidade = sc.nextInt();

        for (int i = 0; i < quantidade; i++) {
            sc.nextLine();
            System.out.print("Digite o nome do hóspede: ");
            String nome = sc.nextLine();
            System.out.print("Digite o email do hóspede: ");
            String email = sc.nextLine();
            System.out.print("Digite o número do quarto: ");
            int quarto = sc.nextInt();
            System.out.println();

            reserva[quarto] = new Reserva(nome, email);
        }

        for (int i = 0; i < reserva.length; i++) {
            if (reserva[i] != null) {
                System.out.println(i + ": " + reserva[i].getNome() + ", " + reserva[i].getEmail());
            }
        }

        sc.close();

    }

}
