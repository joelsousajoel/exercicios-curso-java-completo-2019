package sec10.ex02.app;

import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.Scanner;
import sec10.ex02.entidades.Empregados;
import java.util.List;

public class ProgramaEmpresa {

    public static void main(String[] args) {

        Scanner sc  = new Scanner(System.in);

        List<Empregados> lista = new ArrayList<>();

        System.out.print("Quantos empregados serão registrados: ");
        int quantidadeFuncionarios = sc.nextInt();

        for (int i = 1; i <= quantidadeFuncionarios; i++) {
            System.out.println("Funcionário #" + i + ":");
            System.out.print("ID: ");
            int id = sc.nextInt();
            System.out.print("Nome: ");
            sc.nextLine();
            String nome = sc.nextLine();
            System.out.print("Salario: ");
            double salario = sc.nextDouble();
            System.out.println();

            lista.add(new Empregados(id, nome, salario));
        }

        System.out.println();
        System.out.print("Digite o id do funcionário para aumentar o salário: ");
        int id = sc.nextInt();

        Empregados empregado = lista.stream().filter(x -> x.getId() == id).findFirst().orElse(null);

        if (empregado == null) {
            System.out.println("Id Inexistente!");
        }
        else {
            System.out.print("Digite ao percentual: ");
            int percentual = sc.nextInt();

            empregado.aumentoSalario(percentual);
        }

        System.out.println();
        System.out.println("Lista de empregados: ");

        for (Empregados obj: lista) {
            System.out.println(obj);
        }


        sc.close();



    }

}
