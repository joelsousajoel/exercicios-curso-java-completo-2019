package sec10.ex02.entidades;

public class Empregados {

    private Integer id;
    private String nome;
    private Double salario;

    public Empregados(Integer id, String nome, Double salario) {
        this.id = id;
        this.nome = nome;
        this.salario = salario;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Double getSalario() {
        return salario;
    }

    public void aumentoSalario(int percentual) {
        salario += salario * percentual / 100;
    }

    public String toString() {
        return id + ", " + nome + ", R$ " + salario;
    }

}
