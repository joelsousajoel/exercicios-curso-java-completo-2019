package sec10.ex03.app;

import java.util.Scanner;

public class ProgramaMatriz {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        System.out.print("Digite o tamanho da matriz [] []: ");
        int nLinhas = sc.nextInt();
        int nColunas = sc.nextInt();

        int[][] matriz1 = new int[nLinhas][nColunas];

        for(int i = 0; i < matriz1.length; i++) {

            for(int j = 0; j < matriz1[i].length; j++) {
                System.out.print("Digite o valor da posição (" + i + ", " + j + "): " );
                matriz1[i][j] = sc.nextInt();
            }

        }

        System.out.println();
        System.out.print("Digite um número para consultar na matriz: ");
        int consulta = sc.nextInt();

        for(int i = 0; i < matriz1.length; i++) {

            for(int j = 0; j < matriz1[i].length; j++) {

                if(matriz1[i][j] == consulta) {

                    System.out.println();
                    System.out.println("Posição " + i + ", " + j);
                }
            }
        }

        sc.close();

    }
}
